import { Component, OnInit } from '@angular/core';
import { delay } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { CuentaService } from '../cuenta/cuenta.service';
import { Usuario } from '../models/Usuario.model';
import { SolicitudesService } from '../services/solicitudes.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  currentUser = new Usuario();

  constructor(private cuentaService: CuentaService, private solicitudesService: SolicitudesService, private authService: AuthService) { }

  async ngOnInit() {
    this.authService.autoLogin();
    let user = await this.cuentaService.fetchData();
    this.currentUser = user[0];

  } 

}
