import { Component, OnInit } from '@angular/core';
import { NgForm, FormsModule } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  isLoading = false;
  error: string = null;

  constructor(private authService: AuthService, private router: Router){}
  


  onSubmitLogin(form: NgForm){
    
    if(!form.valid){
      return;
    }

    const email = form.value.email;
    const password = form.value.password;

    this.isLoading= true;

    this.authService.login(email,password).subscribe(
      resData => {
      console.log(resData);
      this.isLoading=false;
      this.router.navigate(['/inicio']);
      console.log(this.authService.user);
      }, 
      error => {
        this.error = 'Los datos ingresados no son correctos. Ingreselos nuevamente.'        
        this.isLoading=false;
      }
    );

    form.reset();
  }
}
