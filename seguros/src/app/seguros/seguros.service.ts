import { EventEmitter, Injectable, OnInit } from '@angular/core';
import { SeguroAPI } from '../models/seguroAPI.model';
import { Compania } from '../models/compania.model';
import { HttpClient } from '@angular/common/http';
import { CuentaService } from '../cuenta/cuenta.service';


@Injectable()
export class SegurosService{
    

    public seguros: Array<SeguroAPI> = [];

    public companies: Array<String> = [];

    public dni: number;

    public matricula : number;

    seguroSelected = new EventEmitter<SeguroAPI>();

    constructor(private http: HttpClient, private cuentaService: CuentaService){

    }   
    

    getSeguro(index: number){
        return this.seguros[index];
    }

    async getSeguros(){
                
        let userData = await this.cuentaService.fetchData();
        let companias = await this.getApisCompanias();

        companias.forEach(element => {
            this.companies.push(element.apiUrl);
        });

        if (userData[0].tipoUsuarioId == 1) {
            this.companies.forEach(async api => {
                          
            let segurosByDni = await this.http.get<Array<SeguroAPI>>(api+'?asegurado.dni='+userData[0].dni).toPromise();
    
            segurosByDni.forEach(element => {
                this.seguros.push(element);
             });
    
                
    
            });
        }else{
            this.companies.forEach(async api => {
                          
                let segurosByMatricula = await this.http.get<Array<SeguroAPI>>(api+'?asegurado.matricula='+userData[0].matricula).toPromise();
        
                segurosByMatricula.forEach(element => {
                    this.seguros.push(element);
                 });
        
                    
        
                });
        }
    
        return this.seguros;
    }


    async getApisCompanias(){
        return await this.http.get<Array<Compania>>('http://localhost:3000/compania').toPromise();
    }
}