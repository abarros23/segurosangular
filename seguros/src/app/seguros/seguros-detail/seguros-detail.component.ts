import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SegurosService } from '../seguros.service';
import { SeguroAPI } from 'src/app/models/seguroAPI.model';



@Component({
  selector: 'app-seguros-detail',
  templateUrl: './seguros-detail.component.html',
  styleUrls: ['./seguros-detail.component.css']
})
export class SegurosDetailComponent implements OnInit {

  seguro : SeguroAPI;
  id: number;

  constructor(private segurosService: SegurosService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params
    .subscribe(
      (params: Params) =>{
        this.id = +params.id;
        this.seguro = this.segurosService.getSeguro(this.id);
      }
    )
  }

}
