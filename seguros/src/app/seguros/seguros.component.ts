import { Component, OnInit } from '@angular/core';

import {SeguroAPI} from '../models/seguroAPI.model'
import { SegurosService } from './seguros.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-seguros',
  templateUrl: './seguros.component.html',
  styleUrls: ['./seguros.component.css'],
  providers: [SegurosService]
})
export class SegurosComponent implements OnInit {

  selectedSeguro: SeguroAPI;

  constructor(private seguroService: SegurosService, private authService: AuthService) { }

  ngOnInit() {
    this.authService.autoLogin();
  }

}
