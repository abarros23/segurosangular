import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { SeguroAPI } from 'src/app/models/seguroAPI.model';
import { SegurosService } from '../seguros.service';

@Component({
  selector: 'app-seguros-list',
  templateUrl: './seguros-list.component.html',
  styleUrls: ['./seguros-list.component.css']
})
export class SegurosListComponent implements OnInit {

  seguros: SeguroAPI[];
  isEmpty: boolean = false;

  constructor(private segurosService: SegurosService,
              private router: Router,
              private route: ActivatedRoute,
              private authService: AuthService) { 

  }

  async ngOnInit() {
    this.authService.autoLogin();
    let seguros = await this.segurosService.getSeguros();   
    this.seguros = seguros;
  }

}
