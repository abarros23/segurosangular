import { Component, OnInit, Input } from '@angular/core';

import {SeguroAPI} from '../../../models/seguroAPI.model';

@Component({
  selector: 'app-seguros-item',
  templateUrl: './seguros-item.component.html',
  styleUrls: ['./seguros-item.component.css']
})
export class SegurosItemComponent implements OnInit {

  @Input() seguro: SeguroAPI;
  @Input() index: number; 
 
  

  ngOnInit() {
  }

}
