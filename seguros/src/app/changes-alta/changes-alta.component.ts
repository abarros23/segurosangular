import { getLocaleDayNames } from '@angular/common';
import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { CuentaService } from '../cuenta/cuenta.service';
import { SegurosService } from '../seguros/seguros.service';
import { GeneralService } from '../services/general.service';

@Component({
  selector: 'app-changes-alta',
  templateUrl: './changes-alta.component.html',
  styleUrls: ['./changes-alta.component.css']
})
export class ChangesAltaComponent implements OnInit {

  public productores: Array<String> = [""];

  public companies: Array<String> = [""];
  
  isLoading = false;
  error: string = null;
  created = false;

  constructor(private cuentaService: CuentaService, private seguroService: SegurosService, private generalService: GeneralService, private authService: AuthService) { }

  async ngOnInit() { 
    this.authService.autoLogin();   
    let tipoUsuarioProductor = await this.generalService.getIdTipoUsuario("Productor");
    var tipoProductor = tipoUsuarioProductor[0].id;
    let productoresList = await this.generalService.getProductores(tipoProductor);
    let companiasList = await this.seguroService.getApisCompanias();

        console.log(productoresList);
        console.log(companiasList);

        companiasList.forEach(element => {
          this.companies.push(element.nombre);
        }); 
        
        productoresList.forEach(element =>{
          this.productores.push(element.nombre+" "+element.apellido+" - MAT."+element.matricula);
        })        

  }

  async onSubmitAlta(form: NgForm){
    let tipoUsuarioProductor = await this.generalService.getIdTipoUsuario("Productor");
    var tipoProductor = tipoUsuarioProductor[0].id;
    let productoresList = await this.generalService.getProductores(tipoProductor);
    this.isLoading= true;
    console.log(form.value);
    const tipoSeguro = form.value.tipoSeguro;
    const compania = form.value.compañia;
    const productor = form.value.productor;
    const detalles = form.value.detalles;
    var productorId;
    productoresList.forEach(element => {
      if (productor.includes(element.matricula.toString())) {
        productorId = element.id;
        console.log(productorId);
      }
    });
    
    let tipoSolicitudSelected = await this.generalService.getIdTipoSolicitud("Alta");
    let companiaSelected = await this.generalService.getIdCompania(compania); 
    let userData = await this.cuentaService.fetchData();
    let estado = await this.generalService.getIdEstado("En Proceso");

    const email = userData[0].id;  
    const solicitudId = Math.random().toString(36).substr(2, 9);
    const seguroId = Math.random().toString(36).substr(2, 9);
    const tipoSolicitudId = tipoSolicitudSelected[0].id;
    const companiaId = companiaSelected[0].id;
    const estadoId = estado[0].id;

    var date = new Date();

    this.generalService.createSolicitudCambio(solicitudId, seguroId, tipoSolicitudId, companiaId, detalles).subscribe(
      resData => {
         
      }, 
      errorMessage => {
        this.error = errorMessage;
        this.created = false;
        this.isLoading=false;
      }
    );
    this.generalService.createSeguro(seguroId, tipoSeguro, "En Progreso", companiaId, detalles).subscribe(
      resData => {
            
      }, 
      errorMessage => {
        this.error = errorMessage;
        this.created = false;
        this.isLoading=false;
      }
    ); 
    this.generalService.createUsuarioSolicitudCambio(email, solicitudId).subscribe(
      resData => {
           
      }, 
      errorMessage => {
        
      }
    );
    this.generalService.createUsuarioSolicitudCambio(productorId, solicitudId).subscribe(
      resData => {
           
      }, 
      errorMessage => {
        
      }
    );
    this.generalService.createUsuarioSeguro(email, seguroId).subscribe(
      resData => {
           
      }, 
      errorMessage => {
        this.error = errorMessage;
        this.created = false;
        this.isLoading=false;
      }
    );

    this.generalService.createEstadoSolicitud(solicitudId, estadoId, date.toLocaleString()).subscribe(
      resData => {
      this.isLoading=false;
      this.error = null;
      this.created = true;      
      }, 
      errorMessage => {
        this.error = errorMessage;
        this.created = false;
        this.isLoading=false;
      }
    );

    this.enviarNotificacion();
    
  }

  enviarNotificacion(){
    if (confirm('Enviar notificación al productor?')) {
      // Save it!
      console.log('Notificación Enviada!');
    } else {
      // Do nothing!
      console.log('');
    }
  }
}
