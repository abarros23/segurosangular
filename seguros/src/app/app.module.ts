import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SegurosComponent } from './seguros/seguros.component';
import { SegurosListComponent } from './seguros/seguros-list/seguros-list.component';
import { SegurosDetailComponent } from './seguros/seguros-detail/seguros-detail.component';
import { SegurosItemComponent } from './seguros/seguros-list/seguros-item/seguros-item.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AppRoutingModule } from './app-routing.module';
import { CuentaComponent } from './cuenta/cuenta.component';
import { SegurosStartComponent } from './seguros-start/seguros-start.component';
import { SegurosService } from './seguros/seguros.service';
import { ChangesComponent } from './changes/changes.component';
import { ChangesAltaComponent } from './changes-alta/changes-alta.component';
import { ChangesBajaComponent } from './changes-baja/changes-baja.component';
import { ChangesModificacionComponent } from './changes-modificacion/changes-modificacion.component'
import { HttpClientModule } from '@angular/common/http';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './auth.service';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { CuentaService } from './cuenta/cuenta.service';
import { GeneralService } from './services/general.service';

import { SolicitudesService } from './services/solicitudes.service';
import { SolicitudesGeneradasComponent } from './solicitudes-generadas/solicitudes-generadas.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SegurosComponent,
    SegurosListComponent,
    SegurosDetailComponent,
    SegurosItemComponent,
    HomepageComponent,
    CuentaComponent,
    SegurosStartComponent,
    ChangesComponent,
    ChangesAltaComponent,
    ChangesBajaComponent,
    ChangesModificacionComponent,
    RegistrationComponent,
    LoginComponent,
    LoadingSpinnerComponent,
    SolicitudesGeneradasComponent    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [SegurosService, AuthService, CuentaService, GeneralService, SolicitudesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
