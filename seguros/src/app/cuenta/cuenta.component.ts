import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CuentaService } from './cuenta.service';
import { Usuario } from '../models/Usuario.model';
import { GeneralService } from '../services/general.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-cuenta',
  templateUrl: './cuenta.component.html',
  styleUrls: ['./cuenta.component.css']
})
export class CuentaComponent implements OnInit {

  currentUser = new Usuario();

  constructor(private cuentaService: CuentaService, private authService: AuthService) { }

  async ngOnInit() {
    this.authService.autoLogin();
    let user = await this.cuentaService.fetchData();
    this.currentUser = user[0];
  } 

}
