import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';
import { Usuario } from '../models/Usuario.model';
import { UserAuth } from '../models/userAuth.model';



@Injectable()
export class CuentaService{

    constructor(private http: HttpClient, private authService: AuthService){}

    email : string = null;
    userObservable : Subscription = this.authService.user.pipe(take(1)).subscribe(user=>{
        this.email = user.email;
    }
    );

    async fetchData(){        
                return await this.http.get<Array<Usuario>>(
                    'http://localhost:3000/usuario',
                    {
                        params: new HttpParams().set('id', this.email)
                    }
                ).toPromise();
    }

    createAccount(email: string, name: string, lastname: string, dni: number, usertype: number, matricula: number, domicilio: string, telefono: number){
        return this.http
        .post('http://localhost:3000/usuario',
            {
                nombre: name,
                apellido: lastname, 
                dni: dni,
                matricula: matricula,
                domicilio: domicilio,
                tipoUsuarioId: usertype,
                id: email,
                telefono: telefono
            }
        );
    }        

}