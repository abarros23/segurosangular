import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { CuentaComponent } from './cuenta/cuenta.component';
import { SegurosComponent } from './seguros/seguros.component';
import { SegurosStartComponent } from './seguros-start/seguros-start.component';
import { SegurosDetailComponent } from './seguros/seguros-detail/seguros-detail.component';
import { ChangesComponent } from './changes/changes.component';
import { ChangesAltaComponent } from './changes-alta/changes-alta.component';
import { ChangesModificacionComponent } from './changes-modificacion/changes-modificacion.component';
import { ChangesBajaComponent } from './changes-baja/changes-baja.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { SolicitudesGeneradasComponent } from './solicitudes-generadas/solicitudes-generadas.component';

const appRoutes : Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'inicio', component: HomepageComponent },
    { path: 'cuenta', component: CuentaComponent },
    { path: 'registro', component: RegistrationComponent },
    { path: 'seguros', component: SegurosComponent,
     children: [
        {path: '', component: SegurosStartComponent},
        {path: ':id', component: SegurosDetailComponent},
    ] },
    { path: 'solicitudes', component: SolicitudesGeneradasComponent },
    { path: 'solicitarCambios', component: ChangesComponent },
    { path: 'solicitarCambios/alta', component: ChangesAltaComponent},
    { path: 'solicitarCambios/modificacion', component: ChangesModificacionComponent},
    { path: 'solicitarCambios/baja', component: ChangesBajaComponent},
    { path: 'login', component: LoginComponent}, 
];


@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})

export class AppRoutingModule{

}