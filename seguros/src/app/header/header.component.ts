import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-header',
    templateUrl:'./header.component.html',
    styleUrls:['./header.component.css'],
})


export class HeaderComponent implements OnInit{
    collapsed = true;

    private userSub: Subscription;
    isAuthenticated = false;

    constructor(private authService: AuthService){}

    ngOnInit(){
        this.userSub = this.authService.user.subscribe(user => {
            this.isAuthenticated = !user ? false : true;
        });
    }

    onLogout(){
        this.authService.logout();
    }
}