import { Component, OnInit } from '@angular/core';
import { combineAll } from 'rxjs/operators';
import { CuentaService } from '../cuenta/cuenta.service';
import { Compania } from '../models/compania.model';
import { SeguroApp } from '../models/seguroApp.model';
import { SolicitudCambio } from '../models/solicitudCambio.model';
import { TipoSolicitud } from '../models/tipoSolicitud.model';
import { Usuario } from '../models/Usuario.model';
import { GeneralService } from '../services/general.service';
import { SolicitudesService } from '../services/solicitudes.service';

@Component({
  selector: 'app-solicitudes-generadas',
  templateUrl: './solicitudes-generadas.component.html',
  styleUrls: ['./solicitudes-generadas.component.css']
})
export class SolicitudesGeneradasComponent implements OnInit {

  public solicitudes: Array<SolicitudCambio> = [];
  public solicitudesId: Array<String> = [];
  public seguroId: Array<String> = [];
  public seguroTipo: Array<String> = [];
  public seguroEstado: Array<String> = [];
  public companiaNombre: Array<String> = [];
  public tipoSolicitudNombre: Array<String> = [];
  public seguroDetalles: Array<String> = [];
  public isLoading = false;
  public asegurado: String = null;
  public productor: String = null;
  public asegurados: Array<String> = [];
  public productores: Array<String> = [];
  currentUser = new Usuario();

  constructor(private solicitudesService: SolicitudesService, private cuentaService: CuentaService, private generalService: GeneralService) { }

  async ngOnInit() {
    this.isLoading = true;
    let userData = await this.cuentaService.fetchData();  
    this.currentUser = userData[0]; 

    let solicitudesUsuario = await this.generalService.getSolicitudUsuarioListByUsuarioId(userData[0].id);
    
    solicitudesUsuario.forEach(async solicitudUsuario => {
      
      let solicitud = await this.solicitudesService.getSolicitudById(solicitudUsuario.solicitudCambioId); 
      this.solicitudes.push(solicitud[0]);
      this.solicitudesId.push(solicitud[0].id);
      
      let usuarios = await this.generalService.getSolicitudUsuarioListBySolicitudId(solicitudUsuario.solicitudCambioId);
      console.log(usuarios);
      if (usuarios[0].usuarioEmail != userData[0].id) {
        let secondUser = await this.generalService.getUsuarioById(usuarios[0].usuarioEmail);
        console.log(secondUser);
        if (secondUser[0].tipoUsuarioId == 1) {
          var asegurado = secondUser[0].nombre+" "+secondUser[0].apellido+" ("+secondUser[0].id+")";
          this.asegurados.push(asegurado);
        }else{
          var productor = secondUser[0].nombre+" "+secondUser[0].apellido+" ("+secondUser[0].id+")";
          this.productores.push(productor);
        }
      }else{
        let secondUser = await this.generalService.getUsuarioById(usuarios[1].usuarioEmail);
        console.log(secondUser);
        if (secondUser[0].tipoUsuarioId == 1) {
          var asegurado = secondUser[0].nombre+" "+secondUser[0].apellido+" ("+secondUser[0].id+")";
          this.asegurados.push(asegurado);
        }else{
          var productor = secondUser[0].nombre+" "+secondUser[0].apellido+" ("+secondUser[0].id+")";
          this.productores.push(productor);
        }
      }
      solicitud.forEach(async element =>{        
        let seguro = await this.generalService.getSeguroById(element.seguroId);       
        this.seguroId.push(seguro[0].id);
        this.seguroEstado.push(seguro[0].estado);
        this.seguroTipo.push(seguro[0].tipo);
        this.seguroDetalles.push(seguro[0].detalle);
      });
      
      solicitud.forEach(async element => {       
        let compania = await this.generalService.getNombreCompania(element.companiaId);
        this.companiaNombre.push(compania[0].nombre);
      });

      solicitud.forEach(async element => {
        let tipoSolicitud = await this.generalService.getnombreTipoSolicitud(element.tipoSolicitudId);
        this.tipoSolicitudNombre.push(tipoSolicitud[0].tipo);
      });
      

    });
    this.isLoading = false;
  }

}
