import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { CuentaService} from '../cuenta/cuenta.service';
import { GeneralService } from '../services/general.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  userTypes =['Productor', 'Asegurado'];

  isLoading = false;
  error: string = null;
  registered = false;

  constructor(private authService: AuthService, private accountService: CuentaService, private generalService: GeneralService){}

  ngOnInit() {}

  async onSubmitSignUp(form: NgForm){
    
    if(!form.valid){
      return;
    }

    const email = form.value.email;
    const password = form.value.password;
    const name = form.value.name;
    const lastname = form.value.lastname;
    const dni = form.value.dni;
    const usertypeSelected = form.value.usertypes;
    const matricula = form.value.matricula;
    const domicilio = form.value.address;
    const telefono = form.value.phone;

    let usertype = await this.generalService.getIdTipoUsuario(usertypeSelected);
    var usertypeId = usertype[0].id;
    this.isLoading= true;
    this.authService.signup(email,password).subscribe(
      resData => {
      this.isLoading=false;
      this.error = null;
      this.registered = true;
      this.onCreateAccount(email, password, name, lastname, dni, usertypeId, matricula, domicilio, telefono);
      }, 
      errorMessage => {
        this.error = errorMessage;
        this.registered = false;
        this.isLoading=false;
      }
    );

    form.reset();
  }

  onCreateAccount(email: string, password: string, name: string, lastname: string, dni: number, usertype: number, matricula: number, domicilio: string, telefono: number){
    this.accountService.createAccount(email, name, lastname, dni, usertype, matricula, domicilio, telefono).subscribe(
      accountData =>{        
        this.error=null;
        this.registered=true;
      }
    )
  }

}
