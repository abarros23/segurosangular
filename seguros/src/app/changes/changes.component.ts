import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-changes',
  templateUrl: './changes.component.html',
  styleUrls: ['./changes.component.css'],
})
export class ChangesComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.autoLogin();
  }

}
