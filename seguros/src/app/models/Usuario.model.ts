export class Usuario{
    public nombre : string;
    public apellido : string;
    public dni : number;
    public matricula : number;
    public domicilio: string;
    public tipoUsuarioId : number;
    public id : string;
    public telefono : number;

    constructor(){}
}