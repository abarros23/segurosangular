import { SeguroDetalle } from './seguroDetalle.model';
import { UserResponse } from './UserResponse.model';

export class SeguroAPI{
    public id : number;
    public tipoSeguro : String;
    public estado : String;
    public compania : String;
    public asegurado : UserResponse;
    public productor : UserResponse;
    public detalles : SeguroDetalle [];

    
    constructor (){}
    
}
