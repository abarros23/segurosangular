export class UserResponse{
    public nombre : string;
    public apellido : string;
    public dni : number;
    public matricula : number;
    public domicilio: string;
    public telefono : number;

    constructor(){}
}