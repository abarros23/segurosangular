import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { catchError} from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AuthService } from '../auth.service';
import { Usuario } from '../models/Usuario.model'
import { TipoSolicitud } from '../models/tipoSolicitud.model';
import { Compania } from '../models/compania.model';
import { Estado } from '../models/estado.model';
import { TipoUsuario } from '../models/tipoUsuario.model';
import { UsuarioSolicitudCambio } from '../models/usuarioSolicitudCambio.model';
import { SeguroApp } from '../models/seguroApp.model';
import { EstadoSolicitud } from '../models/estadoSolicitud.model';
import { UserResponse } from '../models/UserResponse.model';



@Injectable()
export class GeneralService{

    constructor(private http: HttpClient, private authService: AuthService){}
       
    

    createSolicitudCambio(id: string, seguroId: string, tipoSolicitudId: number, companiaId: number, detalles: string){
        
        return this.http.post('http://localhost:3000/solicitudCambio',
        {
            id: id,
            seguroId: seguroId,
            tipoSolicitudId: tipoSolicitudId,
            companiaId: companiaId,
            detalles: detalles
        })
        .pipe(
            catchError(errorRes => {
                let errorMessage = 'No puede procesarse su solicitud en este momento. Intente nuevamente más tarde';            
                return throwError(errorMessage);
            })
        );
    }

    createSeguro(seguroId: string, tipo: string, estado: string, companiaId: number, detalle: string){
        return this.http.post('http://localhost:3000/seguro',
        {
            id: seguroId,
            tipo: tipo,
            estado: estado,
            companiaId: companiaId,
            detalle: detalle
        });
    }

    createUsuarioSolicitudCambio(usuarioId: string, solicitudId: string){
        
        return this.http.post('http://localhost:3000/usuarioSolicitud',
        {
            usuarioEmail: usuarioId,
            solicitudCambioId: solicitudId
        });
    }

    createUsuarioSeguro(usuarioId: string, seguroId: string){
        
        return this.http.post('http://localhost:3000/usuarioSeguro',
        {
            usuarioEmail: usuarioId,
            seguroId: seguroId
        });
    }

    createEstadoSolicitud(solicitudId: string, estadoId: number, fecha: string){
        return this.http.post('http://localhost:3000/estadoSolicitud',
        {
            solicitudCambioId: solicitudId,
            estadoId: estadoId,
            fecha: fecha
        });
    }

    async getEstadoSolicitudById(idSolicitud: string){        
        return await this.http.get<Array<EstadoSolicitud>>(
            'http://localhost:3000/estadoSolicitud',
            {
                params: new HttpParams().set('solicitudCambioId', idSolicitud)
            }
        ).toPromise();
    }

    async getUsuarioById(usuarioId: string){        
        return await this.http.get<Array<Usuario>>(
            'http://localhost:3000/usuario',
            {
                params: new HttpParams().set('id', usuarioId)
            }
        ).toPromise();
    }

    async getIdTipoSolicitud(nombreTipoSolicitud: string){        
                return await this.http.get<TipoSolicitud>(
                    'http://localhost:3000/tipoSolicitud',
                    {
                        params: new HttpParams().set('tipo', nombreTipoSolicitud)
                    }
                ).toPromise();
    }

    async getTipoSolicitudes(){        
        return await this.http.get<Array<TipoSolicitud>>(
            'http://localhost:3000/tipoSolicitud').toPromise();
    }

    async getnombreTipoSolicitud(idTipoSolicitud: number){        
        return await this.http.get<TipoSolicitud>(
            'http://localhost:3000/tipoSolicitud?id='+idTipoSolicitud).toPromise();
    }

    async getSeguroById(idSeguro: string){        
        return await this.http.get<SeguroApp>('http://localhost:3000/seguro',
        {
            params: new HttpParams().set('id', idSeguro)
        }
    ).toPromise();
    }

    async getIdCompania(nombreCompania: string){
        return await this.http.get<Compania>('http://localhost:3000/compania',
        {
            params: new HttpParams().set('nombre', nombreCompania)
        }
    ).toPromise();
    }

    async getCompanias(){
        return await this.http.get<Array<Compania>>('http://localhost:3000/compania').toPromise();
    }

    async getNombreCompania(idCompania: number){
        return await this.http.get<Compania>('http://localhost:3000/compania?id='+idCompania).toPromise();
    }
    
    async getIdEstado(nombreEstado: string){
        return await this.http.get<Estado>('http://localhost:3000/estado',
        {
            params: new HttpParams().set('nombre', nombreEstado)
        }
    ).toPromise();
    }

    async getEstadoById(idEstado: number){
        return await this.http.get<Estado>('http://localhost:3000/estado?id='+idEstado).toPromise();
    }

    async getProductores(tipoUsuarioProductor: number){
        return await this.http.get<Array<Usuario>>(
            'http://localhost:3000/usuario?tipoUsuarioId='+tipoUsuarioProductor)
        .toPromise();
    }

    async getIdTipoUsuario(nombreTipoUsuario: string){
        return await this.http.get<TipoUsuario>('http://localhost:3000/tipoUsuario',
        {
            params: new HttpParams().set('nombre', nombreTipoUsuario)
        }
    ).toPromise();
    }

    async getNombreTipoUsuario(idTipoUsuario: number){
        return await this.http.get<TipoUsuario>('http://localhost:3000/tipoUsuario?id='+idTipoUsuario).toPromise();
    }

    async getSolicitudUsuarioListByUsuarioId(usuarioId: string){
        
        return this.http.get<Array<UsuarioSolicitudCambio>>('http://localhost:3000/usuarioSolicitud',
        {
            params: new HttpParams().set('usuarioEmail', usuarioId)
        }
    ).toPromise();
    }


    async getSolicitudUsuarioListBySolicitudId(solicitudId: string){
        
        return this.http.get<Array<UsuarioSolicitudCambio>>('http://localhost:3000/usuarioSolicitud',
        {
            params: new HttpParams().set('solicitudCambioId', solicitudId)
        }
    ).toPromise();
    }
}