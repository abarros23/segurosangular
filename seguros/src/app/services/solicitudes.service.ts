import { HttpClient, HttpParams } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { SolicitudCambio } from '../models/solicitudCambio.model';
import { GeneralService } from './general.service';
import { CuentaService } from '../cuenta/cuenta.service';

@Injectable()
export class SolicitudesService {

  public solicitudes: Array<SolicitudCambio> = [];

  solicitudSelected = new EventEmitter<SolicitudCambio>();

  constructor(private http: HttpClient, private generalService: GeneralService, private cuentaService: CuentaService){

  }   
  

  getSolicitud(index: number){
      return this.solicitudes[index];
  }

  async getSolicitudes(userId : string){
 
      let solicitudesUsuario = await this.generalService.getSolicitudUsuarioListByUsuarioId(userId);
      solicitudesUsuario.forEach(async solicitudUsuario => {
        let solicitud = await this.getSolicitudById(solicitudUsuario.solicitudCambioId);        
        this.solicitudes.push(solicitud[0]);
      })      
      console.log(this.solicitudes);      
      return this.solicitudes;
  }

  async getSolicitudById(solicitudId: string){
    return this.http.get<Array<SolicitudCambio>>('http://localhost:3000/solicitudCambio',
        {
            params: new HttpParams().set('id', solicitudId)
        }
    ).toPromise();
  }
}
